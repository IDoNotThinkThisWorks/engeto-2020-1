package org.engeto.lesson11;

import java.util.Scanner;

public class Runner {
    public static void main(String[] args) {
        DirectoryReader reader = new DirectoryReader("D:\\Projects\\lessons-2020-1");
        CommandReader commandReader = new CommandReader(reader);
        commandReader.startReading();
    }
}
