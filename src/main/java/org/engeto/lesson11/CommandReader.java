package org.engeto.lesson11;

import java.util.Scanner;

public class CommandReader {
    private static final String EXIT_COMMAND = "exit";
    private static final String LIST_COMMAND = "list";
    private static final String CHANGE_DIR_COMMAND = "cd";
    private static final String HEAD_COMMAND = "head";
    private static final String NEW_FILE_COMMAND = "new";
    private static final String INSERT_LINE_COMMAND = "insert";

    private final CommandListener listener;

    public CommandReader(CommandListener listener) {
        this.listener = listener;
    }

    public void startReading() {
        Scanner inputScanner = new Scanner(System.in);
        while (true) {
            String input = inputScanner.nextLine();
            String[] inputsArray = input.split(" ");

            switch (inputsArray[0]) {
                case EXIT_COMMAND:
                    return;
                case LIST_COMMAND:
                    if (inputsArray.length <= 1) {
                        listener.onList();
                    } else {
                        listener.onListFilter(inputsArray[1]);
                    }

                    break;
                case CHANGE_DIR_COMMAND:
                    listener.changeDirectory(inputsArray[1]);
                    break;
                case HEAD_COMMAND:
                    listener.onHead(inputsArray[1]);
                    break;
                case NEW_FILE_COMMAND:
                    listener.onNewFile(inputsArray[1]);
                    break;
                case INSERT_LINE_COMMAND:
                    listener.onInsertLine(inputsArray[1], inputsArray[2]);
                    break;
                default:
                    listener.onUnknown();
            }
        }
    }
}
