package org.engeto.lesson11;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class DirectoryReader implements CommandListener {
    private File currentFile;

    private final ConsolePrinter printer;

    public DirectoryReader(String rootDirPath) {
        this(rootDirPath, new ConsolePrinter());
    }

    DirectoryReader(String rootDirPath, ConsolePrinter printer) {
        this.currentFile = new File(rootDirPath);
        this.printer = printer;
    }

    @Override
    public void onList() {
        if (currentFile.isDirectory()) {
            File[] files = currentFile.listFiles();

            for (int i = 0; i < files.length; i++) {
                printer.print(getFileName(files[i]));
            }
        }
    }

    @Override
    public void onListFilter(String name) {
        File[] files = currentFile.listFiles();
        if (files == null) {
            return;
        }

        List<File> filesList = asList(files);

        List<File> filteredList = filesList.stream()
                .filter(file -> file.getName().contains(name))
                .collect(Collectors.toList());

        filteredList.forEach(file -> printer.print(getFileName(file)));
    }

    @Override
    public void changeDirectory(String name) {
        File newDir = getFileInCurrentFolder(name);
        if (newDir.exists() && newDir.isDirectory()) {
            this.currentFile = newDir;
        }
    }

    @Override
    public void onUnknown() {
        printer.print("Unknown command");
    }

    @Override
    public void onHead(String fileName) {
        File file = getFileInCurrentFolder(fileName);
        if (file.exists() && !file.isDirectory()) {

            try (Scanner reader = new Scanner(file)) {
                for (int i = 0; i < 15 && reader.hasNextLine(); i++) {
                    printer.print(reader.nextLine());
                }
            } catch (FileNotFoundException e) {
                printer.print("TOTAL ERROR");
            }
        }
    }

    @Override
    public void onNewFile(String fileName) {
        File file = getFileInCurrentFolder(fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            printer.print("Failed to create file");
        }
    }

    @Override
    public void onInsertLine(String fileName, String insertedLine) {
        File file = getFileInCurrentFolder(fileName);
        if (file.exists() && !file.isDirectory()) {
            try {
                Files.write(Path.of(file.getAbsolutePath()), Arrays.asList(insertedLine));
            } catch (IOException e) {
                printer.print("Error: " + e.getMessage());
            }
        }
    }

    private File getFileInCurrentFolder(String fileName) {
        return new File(this.currentFile, fileName);
    }

    private String getFileName(File file) {
        String prefix;
        if (file.isDirectory()) {
            prefix = "D:";
        } else {
            prefix = "F:";
        }

        return prefix + file.getName();
    }
}
