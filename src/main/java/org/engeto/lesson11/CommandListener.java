package org.engeto.lesson11;

public interface CommandListener {
    void onList();

    void onListFilter(String name);

    void changeDirectory(String name);

    void onUnknown();

    void onHead(String fileName);

    void onNewFile(String fileName);

    void onInsertLine(String fileName, String insertedLine);
}
