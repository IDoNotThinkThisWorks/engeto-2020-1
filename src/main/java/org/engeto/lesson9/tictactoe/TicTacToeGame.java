package org.engeto.lesson9.tictactoe;

public class TicTacToeGame {
    private final int size;
    private final FieldValue[][] gameBoard;

    public TicTacToeGame(int size) {
        this.size = size;
        this.gameBoard = new FieldValue[size][size];
    }

    public void init() {
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                gameBoard[x][y] = FieldValue.EMPTY;
            }
        }
    }

    public boolean setField(FieldValue value, int x, int y) {
        if (x >= size || y >= size) {
            return false;
        }

        if (gameBoard[x][y] != FieldValue.EMPTY) {
            return false;
        }

        gameBoard[x][y] = value;
        return true;
    }

    public void printBoard() {
        System.out.println("-".repeat(size * 4));

        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                String toPrint = convertToField(gameBoard[x][y]);
                System.out.print(toPrint + "|");
            }
            System.out.println();
        }

        System.out.println("-".repeat(size * 4));
    }

    private String convertToField(FieldValue value) {
        switch (value) {
            case EMPTY: return " ";
            case O: return "O";
            case X: return "X";
        }

        return "unkown";
    }

    enum FieldValue {
        EMPTY,
        X,
        O
    }
}
