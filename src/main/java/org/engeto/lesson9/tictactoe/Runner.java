package org.engeto.lesson9.tictactoe;

import java.util.Scanner;

public class Runner {
    public static void main(String[] args) {
        TicTacToeGame game = new TicTacToeGame(5);
        game.init();

        Scanner scanner = new Scanner(System.in);
        while (true) {
            game.printBoard();

            System.out.println("Input coordinates:");
            String input = scanner.nextLine();
            if (input.equals("exit")) {
                break;
            }

            String[] inputNumbers = input.split(" ");
            if (inputNumbers.length != 2) {
                System.out.println("Invalid input!");
                continue;
            }

            Integer x = parseInt(inputNumbers[0]);
            Integer y = parseInt(inputNumbers[1]);

            if (x == null || y == null) {
                System.out.println("Invalid input!");
                continue;
            }

            game.setField(TicTacToeGame.FieldValue.O, x, y);
        }
    }

    public static Integer parseInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
