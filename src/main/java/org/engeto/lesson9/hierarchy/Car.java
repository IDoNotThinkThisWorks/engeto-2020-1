package org.engeto.lesson9.hierarchy;

public class Car extends NamedEntity implements Vehicle, Weight {
    public Car(String name) {
        super(name);
    }

    @Override
    protected String getDriver() {
        return "Driver";
    }

    @Override
    public int getWheelCount() {
        return 4;
    }

    public int seats() {
        return 5;
    }

    @Override
    public int getKgs() {
        return 1000;
    }
}
