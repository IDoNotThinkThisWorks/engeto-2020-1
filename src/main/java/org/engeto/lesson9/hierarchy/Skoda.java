package org.engeto.lesson9.hierarchy;

public class Skoda extends Car {
    public Skoda(String name) {
        super(name);
    }

    @Override
    public String getName() {
        return "Skoda " + super.getName();
    }

    @Override
    public int getKgs() {
        return -10;
    }
}
