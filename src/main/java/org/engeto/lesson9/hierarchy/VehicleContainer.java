package org.engeto.lesson9.hierarchy;

public class VehicleContainer {
    private final int size;

    private int vehiclesCacheIndex;
    private final Vehicle[] vehiclesCache;

    private int weightsCacheIndex;
    private final Weight[] weightsCache;

    private int namedEntitiesIndex;
    private final NamedEntity[] namedEntities;

    public VehicleContainer(int size) {
        this.size = size;
        vehiclesCache = new Vehicle[size];
        weightsCache = new Weight[size];
        namedEntities = new NamedEntity[size];
    }

    public void addVehicle(Vehicle vehicle) {
        vehiclesCacheIndex = getNextIndex(vehiclesCacheIndex);
        vehiclesCache[vehiclesCacheIndex] = vehicle;
    }

    public void addWeight(Weight weight) {
        weightsCacheIndex = getNextIndex(weightsCacheIndex);
        weightsCache[weightsCacheIndex] = weight;
    }

    public void printVehicles() {
        System.out.println("Vehicles:");

        for (int i = 0; i < vehiclesCache.length; i++) {
            Vehicle vehicle = vehiclesCache[i];
            if (vehicle != null) {
                System.out.println(vehicle.getName() + " - " + vehicle.getWheelCount());
            }
        }
    }

    public void printWeights() {
        System.out.println("Weights:");

        for (int i = 0; i < weightsCache.length; i++) {
            Weight vehicle = weightsCache[i];
            if (vehicle != null) {
                System.out.println(vehicle.getName() + " - " + vehicle.getKgs());
            }
        }
    }

    private int getNextIndex(int index) {
        if (index >= size - 1) {
            return 0;
        }

        return index + 1;
    }
}
