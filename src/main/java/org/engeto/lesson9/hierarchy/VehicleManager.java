package org.engeto.lesson9.hierarchy;

public class VehicleManager {
    public void doStuff() {
        VehicleContainer container = new VehicleContainer(10);

        container.addVehicle(new Car("Volvo"));
        container.addVehicle(new Bicycle("X"));
        container.addVehicle(new Airplane("Boeing"));
        container.addVehicle(new Car("Lada"));
        container.addVehicle(new Skoda("Fabia"));
        container.addVehicle(new Skoda("Octavia"));

        container.printVehicles();

        container.addWeight(new Car("Lada"));
        container.addWeight(new Skoda("Fabia"));
        container.addWeight(new Airplane("Airbus"));

        container.printWeights();
    }
}
