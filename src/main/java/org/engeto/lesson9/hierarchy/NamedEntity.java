package org.engeto.lesson9.hierarchy;

public abstract class NamedEntity {
    private final String name;

    protected NamedEntity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    protected abstract String getDriver();
}
