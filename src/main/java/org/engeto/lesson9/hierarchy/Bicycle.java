package org.engeto.lesson9.hierarchy;

public class Bicycle extends NamedEntity implements Vehicle {
    public Bicycle(String name) {
        super(name);
    }

    @Override
    protected String getDriver() {
        return "Cyclist";
    }

    @Override
    public int getWheelCount() {
        return 2;
    }
}
