package org.engeto.lesson9.hierarchy;

public interface Vehicle {
    String getName();

    int getWheelCount();
}
