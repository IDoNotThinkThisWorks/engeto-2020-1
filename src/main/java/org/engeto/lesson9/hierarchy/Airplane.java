package org.engeto.lesson9.hierarchy;

public class Airplane extends NamedEntity implements Vehicle, Weight {
    public Airplane(String name) {
        super(name);
    }

    @Override
    public String getDriver() {
        return "Pilot";
    }

    @Override
    public int getWheelCount() {
        return 10;
    }

    @Override
    public int getKgs() {
        return 10000000;
    }

    public int flightHeigh() {
        return 10000;
    }
}
