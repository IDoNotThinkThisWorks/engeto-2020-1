package org.engeto.lesson9.hierarchy;

public interface Weight {
    String getName();

    int getKgs();
}
