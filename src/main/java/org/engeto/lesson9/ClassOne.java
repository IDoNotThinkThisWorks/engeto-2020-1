package org.engeto.lesson9;

public class ClassOne {
    public static final String MESSAGE = "Hello";

    public static String name;

    public ClassOne(String name) {
    }

    public static String getName() {
        return name;
    }

    public static class InnerClass {
        private String name;
    }
}
