package org.engeto.lesson9;

public class Sorting {
    private String[] array = new String[10];

    public void runSorting() {
        array[0] = "A";
        array[1] = "C";
        array[2] = "X";
        array[3] = "F";
        array[4] = "L";
        array[5] = "P";
        array[6] = "W";
        array[7] = "Z";
        array[8] = "D";
        array[9] = "G";

        for(int i = 0; i < array.length; i++) {
            int tmpIndex = i;
            for (int j = i; j < array.length; j++) {
                if (toChar(array[j]) < toChar(array[tmpIndex])) {
                    tmpIndex = j;
                }
            }

            String tmp = array[i];
            array[i] = array[tmpIndex];
            array[tmpIndex] = tmp;
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
        }
        System.out.println();
    }

    public char toChar(String value) {
        if (value != null && !value.isEmpty()) {
            return value.toCharArray()[0];
        }

        return ' ';
    }
}
