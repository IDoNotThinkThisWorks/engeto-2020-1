package org.engeto.lesson9;

public class ClassOneFactory {
    public ClassOne createClassOne(String name) {
        return new ClassOne(name);
    }

    public ClassOne.InnerClass createInnerClass() {
        return new ClassOne.InnerClass();
    }

    public void test() {

    }
}
