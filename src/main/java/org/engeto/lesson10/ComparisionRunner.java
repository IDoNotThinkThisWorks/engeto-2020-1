package org.engeto.lesson10;

import org.engeto.lesson10.container.ListValuesContainer;

public class ComparisionRunner {
    public static void main(String[] args) {
        ListAndSetComparision comparision = new ListAndSetComparision();
        comparision.init();

        long start = System.nanoTime();
        comparision.findInList("249000");
        long endTime = System.nanoTime();
        System.out.println("Find in list took " + (endTime - start));

        long start1 = System.nanoTime();
        comparision.findInSet("249000");
        long endTime1 = System.nanoTime();
        System.out.println("Find in set took " + (endTime1 - start1));
    }
}
