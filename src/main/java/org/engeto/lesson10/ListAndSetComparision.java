package org.engeto.lesson10;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListAndSetComparision {
    private final Set<String> set;
    private final List<String> list;

    public ListAndSetComparision() {
        list = new ArrayList<>();
        set = new HashSet<>();
    }

    public void init() {
        for(int i = 0; i < 250000; i++) {
            list.add(String.valueOf(i));
            set.add(String.valueOf(i));
        }
    }

    public boolean findInList(String value) {
        return list.contains(value);
    }

    public boolean findInSet(String value) {
        return set.contains(value);
    }
}
