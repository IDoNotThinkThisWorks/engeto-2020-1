package org.engeto.lesson10.container;

import java.util.HashMap;
import java.util.Map;

public class MapValuesContainer implements ValuesContainer {
    private final Map<String, String> map;

    public MapValuesContainer() {
        map = new HashMap<>();
    }

    @Override
    public void insert(String value) {
        map.put(String.valueOf(0), value);
    }

    @Override
    public String getLastValue() {
        for(String value : map.keySet()) {
            map.get(value);
        }

        return map.get(String.valueOf(0));
    }

    @Override
    public String getValue(int index) {
        return map.get(String.valueOf(index));
    }

    @Override
    public int count() {
        return map.size();
    }
}
