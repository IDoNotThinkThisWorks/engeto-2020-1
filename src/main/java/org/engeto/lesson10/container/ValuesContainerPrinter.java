package org.engeto.lesson10.container;

public class ValuesContainerPrinter {

    public void printContainer(ValuesContainer container) {
        for (int i = 0; i < container.count(); i++) {
            System.out.println(container.getValue(i));
        }
    }
}
