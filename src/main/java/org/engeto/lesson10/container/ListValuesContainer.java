package org.engeto.lesson10.container;

import java.util.ArrayList;
import java.util.List;

public class ListValuesContainer implements ValuesContainer {
    private final List<String> list;

    public ListValuesContainer() {
        this.list = new ArrayList<>();
    }

    @Override
    public void insert(String value) {
        list.add(0, value);
    }

    @Override
    public String getLastValue() {
        return list.get(0);
    }

    @Override
    public String getValue(int index) {
        return list.get(index);
    }

    @Override
    public int count() {
        return list.size();
    }
}
