package org.engeto.lesson10.container;

/**
 * Container holding {@link String} values specific position.
 */
public interface ValuesContainer {

    /**
     * Insert new value into the container.
     * It should be on the first position.
     * @param value
     */
    void insert(String value);

    /**
     * Return the last element inserted into the container.
     * @return
     */
    String getLastValue();

    /**
     * Return element at given index.
     * @param index
     * @return
     */
    String getValue(int index);

    /**
     * Return how many values does the container hold.
     * @return
     */
    int count();
}
