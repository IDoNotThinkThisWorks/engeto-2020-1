package org.engeto.lesson10.container;

import java.util.HashSet;
import java.util.Set;

public class SetValuesContainer implements ValuesContainer {
    private final Set<String> set;

    public SetValuesContainer() {
        set = new HashSet<>();
    }

    @Override
    public void insert(String value) {
        set.add(value);
    }

    @Override
    public String getLastValue() {
        for (String value : set) {
            return value;
        }

        return null;
    }

    @Override
    public String getValue(int index) {
        for (String value : set) {
            return value;
        }

        return null;
    }

    @Override
    public int count() {
        return set.size();
    }
}
