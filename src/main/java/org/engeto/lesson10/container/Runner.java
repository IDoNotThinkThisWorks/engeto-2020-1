package org.engeto.lesson10.container;

public class Runner {
    public static void main(String[] args) {
        ValuesContainerPrinter printer = new ValuesContainerPrinter();

        try {
            ValuesContainer container = new ArrayValuesContainer();
            container.insert("Hello");
            container.insert("World");
            container.insert("!");
            container.insert("X");
            container.insert("Y");

            printer.printContainer(container);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
