package org.engeto.lesson10.container;

/**
 * Implement class that holds an array of values.
 * There is a method that adds value to the first position in the array
 * and shifts the rest of the array to the right
 * Example:
 *      Array: 9 8 7 6 5
 *      Input: 1
 *      Result: 1 9 8 7 6 5
 */
public class ArrayValuesContainer implements ValuesContainer {
    private final String[] array;

    public ArrayValuesContainer() {
        this.array = new String[4];
    }

    public void insert(String value) {
        if (array[3] != null) {
            throw new RuntimeException("Container already full");
        }

        for (int i = array.length - 1; i > 0; i--) {
            this.array[i] = this.array[i - 1];
        }

        this.array[0] = value;
    }

    public String getLastValue() {
        return this.array[0];
    }

    public String getValue(int index) {
        return this.array[index];
    }

    public int count() {
        for (int i = 0; i < this.array.length; i++) {
            if (this.array[i] == null) {
                return i;
            }
        }

        return this.array.length;
    }
}
