package org.engeto.lesson1;

public class Runner {

    public static void main(String[] args) {
        int length = 5;

        // #1 - Vypiste text delky length, ktery na zacatku a na konci obsahuje X,
        //      a mezi nima '.'
        //      Napr.:  delka 7 => X.....X
        //              delka 3 => X.X
        //              delka 6 => X....X

        for (int i = 0; i < length; i++) {
            if (i == 0 || i >= length - 1) {
                System.out.print("X");
            } else {
                System.out.print(".");
            }
        }
        System.out.println();
        System.out.println();


        // #2 - Vypiste text delky length, ktery na zacatku a na konci obsahuje X,
        //      a uprostred take X. Zybtek text je vyplnen '.'
        //      Napr.:  delka 7 => X..X..X
        //              delka 3 => XXX
        //      Zkuste zvazit co se stane pokud je cislo sude, a jak chovani vysledek pripadne opravit

        for (int i = 0; i < length; i++) {
            if (i == 0 || i >= length - 1) {
                System.out.print("X");
            } else if (i == length / 2) {
                System.out.print("X");
            } else {
                System.out.print(".");
            }
        }
        System.out.println();
        System.out.println();


        // #3 - Vypiste ctverec o delce strany length, ktery po okrajich obsahuje X, a zbytek je vyplnen
        //      znakem '.'
        // Ocekavany vystup:
        //      XXXXX
        //      X...X
        //      X...X
        //      X...X
        //      XXXXX

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                if (i == 0 || i >= length - 1 || j == 0 || j >= length - 1) {
                    System.out.print("X");
                } else {
                    System.out.print(".");
                }
            }
            System.out.println();
        }
        System.out.println();


        // #4 - Vytvorte pole delky length, kde prvni a posledni element bude obsahovat znak X,
        //      zbytek bude obsahovat '.'. Pote pole vypiste

        String[] array4 = new String[length];   // vytvoreni pole delky length

        for (int i = 0; i < length; i++) {      // vsimete se ze algorimus je naprosto stejny jako
            if (i == 0 || i >= length - 1) {    // u prikladu #1, zde pouze pracujeme s polem
                array4[i] = "X";
            } else {
                array4[i] = ".";
            }
        }

        for (int i = 0; i < length; i++) {      // vypis jednorozmerneho pole
            System.out.print(array4[i]);
        }
        System.out.println();
        System.out.println();


        // #5 - Vytvorte dvourozmerne pole delky length (v obou rozmerech), a naplnte jej tak, aby
        //      na krajich obsahovalo X, a stred byl vyplnen '.'. Toto pole pote vytisknete
        // Ocekavany vystup:
        //      XXXXX
        //      X...X
        //      X...X
        //      X...X
        //      XXXXX

        String[][] array5 = new String[length][length];     // vytvoreni dvourozmerneho pole

        for (int i = 0; i < length; i++) {                  // algoritmus stejny jako u prikladu #3
            for (int j = 0; j < length; j++) {
                if (i == 0 || i >= length - 1 || j == 0 || j >= length - 1) {
                    array5[i][j] = "X";
                } else {
                    array5[i][j] = ".";
                }
            }
        }

        for (int i = 0; i < length; i++) {        // vypis dvourozmerneho pole
            for (int j = 0; j < length; j++) {
                System.out.print(array5[i][j]);
            }
            System.out.println();
        }
        System.out.println();


        // #6 - Vytvorte pole ktere obsahuje nahodne znaky X a '.' (zvolte dle libosti).
        //      Napiste algoritmus, ktery bude znaky v poli posouvat zleva do prava.
        // Priklad: Krok 1 - X..XX.X
        //          Krok 2 - XX..XX.
        //          Krok 3 - .XX..XX
        //          Krok 4 - X.XX..X
        //          atd.
        // Poznamka - Nasledujici reseni vyuziva bufferu k zapamatovani prepisovane hodnoty. Pokud vam
        // dela potize algoritmus pochopit, zkuste si postup rozkreslit na papiru

        String[] array6 = new String[]{"X", ".", ".", "X", "X", ".", "X"}; // vytvoreni pole naplneneho hodnotami

        int repeatCount = 5;
        while (repeatCount-- > 0) {     // smycka ridic pocet opakovani

            String buffer = array6[0];
            for (int i = 0; i < array6.length; i++) {   // smycka prochazejici pole (stejne jako v predchozich prikladech)
                int nextIndex = i + 1;                  // vypocet indexu nasledujicho prvku
                if (nextIndex >= array6.length) {       // pokud jiz nasledujici prvek neexistuje, musim se vratit na zacatek pole
                    nextIndex = 0;
                }

                String nextBuffer = array6[nextIndex];  // docasne ulozim hodnotu kterou budu prepisovat
                array6[nextIndex] = buffer;             // prepisi hodnotu predchozi hodnotou
                buffer = nextBuffer;                    // ulozim prepsanou hodnotu
            }

            for (int x = 0; x < array6.length; x++) {
                System.out.print(array6[x]);
            }

            System.out.println();
        }
    }
}
