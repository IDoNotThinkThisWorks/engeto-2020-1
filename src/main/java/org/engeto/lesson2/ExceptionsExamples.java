package org.engeto.lesson2;

public class ExceptionsExamples {
    public static void main(String[] args) {
        try {
            InnerClass value = new InnerClass();
            value.doDivide(1, 1);
            return;
        } catch (MyRuntimeException e) {
            System.out.println("Caught MyRuntimeException");
        } finally {
            System.out.println("Finally");
        }

        System.out.println("Ended");
    }

    public static class InnerClass {
        void doDivide(int i, int j) {
            int result = i / j;
            System.out.println(result);
//            throw new MyRuntimeException();
        }
    }

    public static class MyException extends Exception {
        public MyException() {
            super();
        }

        public void doStuff() {

        }
    }

    public static class MyRuntimeException extends RuntimeException {
    }
}
