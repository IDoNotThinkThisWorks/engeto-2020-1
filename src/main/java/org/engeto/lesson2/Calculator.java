package org.engeto.lesson2;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Double numberA = null;
        Double numberB = null;

        while (numberA == null) {
            System.out.println("Insert number A:");
            String input = in.nextLine();

            numberA = tryParse(input);
        }

        while (numberB == null) {
            System.out.println("Insert number B:");
            String input = in.nextLine();

            numberB = tryParse(input);
        }

        while (true) {
            System.out.println("Insert operator:");
            String input = in.nextLine();

            switch (input) {
                case "+":
                    System.out.println(numberA + numberB);
                    return;
                case "-":
                    System.out.println(numberA - numberB);
                    return;
                case "*":
                    System.out.println(numberA * numberB);
                    return;
                case "/":
                    System.out.println(numberA / numberB);
                    return;
                default:
                    System.out.println("Invalid operator: " + input);
            }
        }
    }

    public static Double tryParse(String input) {
        try {
            return Double.parseDouble(input);
        } catch (NumberFormatException e) {
            System.out.println("Input was invalid:" + input);
        }

        return null;
    }
}
