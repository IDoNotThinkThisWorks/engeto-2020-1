package org.engeto.lesson2;

public class FizzBuzz {
    //TODO:
    // Vypis cisla od 1 do 100
    // Pokud je delitelne 3, vypis misto nej Fizz
    // Pokud je delitelne 5, vypis misto nej Buzz
    // Pokud je delitelne 3 i 5, vypis FizzBuzz

    public static void main(String[] args) {
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0) {
                if (i % 5 == 0) {
                    System.out.println("FizzBuzz");
                }

                System.out.println("Fizz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }
}
