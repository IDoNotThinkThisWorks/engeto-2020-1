package org.engeto.lesson2;

public class CaesarCipher {
    //TODO:
    // Do cipher that shifts each letter in word by specific number
    // of characters
    // Example:
    //      for number 2, 'a' becomes 'c'
    //      for number 5, 'd' becomes 'i'
    //      for number 3, 'hello' becomes 'khoor'

    public void doCipher() {
        String word = "hello";
        for (int index = 0; index < word.length(); index++) {
            char newChar = (char) (word.charAt(index) + 1);
            System.out.println(newChar);
        }
    }

    public void doDecipher() {
        String word = "ifmmp";
    }
}
