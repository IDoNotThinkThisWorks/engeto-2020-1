package org.engeto.lesson12.api.dto;

import java.util.ArrayList;
import java.util.List;

public class LinesDto {
    private List<String> lines;

    public LinesDto(List<String> lines) {
        this.lines = new ArrayList<>(lines);
    }

    public List<String> getLines() {
        return lines;
    }
}
