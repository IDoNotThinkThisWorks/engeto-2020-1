package org.engeto.lesson12.api.dto;

public class DiskEntryDto {
    private String type;
    private String name;

    public DiskEntryDto(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}
