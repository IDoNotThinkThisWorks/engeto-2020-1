package org.engeto.lesson12.api.dto;

import java.util.List;

public class BrowsingResultDto {
    private List<DiskEntryDto> entries;

    public List<DiskEntryDto> getEntries() {
        return entries;
    }

    public void setEntries(List<DiskEntryDto> entries) {
        this.entries = entries;
    }
}
