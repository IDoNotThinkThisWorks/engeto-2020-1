package org.engeto.lesson12.api;

import org.engeto.lesson12.api.dto.SuccessResponseDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PingController {
    @GetMapping("/ping")
    public SuccessResponseDto ping() {
        return new SuccessResponseDto(true);
    }
}
