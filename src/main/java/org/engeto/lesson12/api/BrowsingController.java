package org.engeto.lesson12.api;

import org.engeto.lesson12.api.dto.BrowsingResultDto;
import org.engeto.lesson12.api.dto.DiskEntryDto;
import org.engeto.lesson12.api.dto.LinesDto;
import org.engeto.lesson12.api.dto.SuccessResponseDto;
import org.engeto.lesson12.service.BrowsingService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class BrowsingController {
    private final BrowsingService browsingService;

    public BrowsingController() {
        browsingService = new BrowsingService("C:");
    }

    @GetMapping("/list")
    public BrowsingResultDto list(@RequestParam(required = false) String filter) {
        List<File> contents;
        if (filter != null && filter.length() > 0) {
            contents = browsingService.onListFilter(filter);
        } else {
            contents = browsingService.onList();
        }

        return mapResult(contents);
    }

    @GetMapping("/cd/{dirName}")
    public SuccessResponseDto changeDir(@PathVariable String dirName) {
        boolean result = browsingService.changeDirectory(dirName);
        return new SuccessResponseDto(result);
    }

    @GetMapping("/head/{fileName}")
    public LinesDto headFile(@PathVariable String fileName) {
        List<String> lines = browsingService.onHead(fileName);
        return new LinesDto(lines);
    }

    private BrowsingResultDto mapResult(List<File> files) {
        List<DiskEntryDto> entries = files.stream()
                .map(this::mapEntry)
                .collect(Collectors.toList());

        BrowsingResultDto dto = new BrowsingResultDto();
        dto.setEntries(entries);
        return dto;
    }

    private DiskEntryDto mapEntry(File file) {
        String type = file.isDirectory() ? "directory" : "file";
        return new DiskEntryDto(type, file.getName());
    }
}
