### SPRING BOOT APPLIACTION

#### Endpoints

##### Ping
url: http://localhost:8080/ping

Returns true if the server is running, not accessible otherwise

##### Hello world
url: http://localhost:8080/helloWorld

Returns "Hello World" string

##### Browse
###### http://localhost:8080/list
Lists content of the root folder (set up in the controller)

###### http://localhost:8080/cd/<directory_name>

example: http://localhost:8080/cd/Windows

Changes current root folder

###### http://localhost:8080/head/<file_name>

example: http://localhost:8080/head/README.md

Returns first 15 lines from file in current root folder