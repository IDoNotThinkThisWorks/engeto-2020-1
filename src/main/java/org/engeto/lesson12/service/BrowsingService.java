package org.engeto.lesson12.service;

import java.io.File;
import java.io.FileNotFoundException;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class BrowsingService {
    private File currentFile;

    public BrowsingService(String rootDirPath) {
        this.currentFile = new File(rootDirPath);
    }

    public List<File> onList() {
        if (currentFile.isDirectory()) {
            File[] files = currentFile.listFiles();

            return Arrays.asList(files);
        }

        return Collections.emptyList();
    }

    public List<File> onListFilter(String name) {
        File[] files = currentFile.listFiles();
        if (files == null) {
            return Collections.emptyList();
        }

        List<File> filesList = asList(files);

        return filesList.stream()
                .filter(file -> file.getName().contains(name))
                .collect(Collectors.toList());
    }

    public boolean changeDirectory(String name) {
        File newDir = getFileInCurrentFolder(name);
        if (newDir.exists() && newDir.isDirectory()) {
            this.currentFile = newDir;
            return true;
        }

        return false;
    }

    public List<String> onHead(String fileName) {
        File file = getFileInCurrentFolder(fileName);
        if (file.exists() && !file.isDirectory()) {
            try (Scanner reader = new Scanner(file)) {

                List<String> result = new ArrayList<>();
                for (int i = 0; i < 15 && reader.hasNextLine(); i++) {
                    result.add(reader.nextLine());
                }

                return result;
            } catch (FileNotFoundException ignored) {
            }
        }

        return Collections.emptyList();
    }

    private File getFileInCurrentFolder(String fileName) {
        return new File(this.currentFile, fileName);
    }
}
