package org.engeto.lesson11;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class DirectoryReaderTest {
    private static final String TEST_FOLDR = "test-folder";
    private static final String SUB_DIR_NAME = "sub-dir-1";
    private static final String SUB_DIR_2_NAME = "sub-dir-2";
    private static final String SUB_FILE_NAME = "file-1";
    private static final String SUB_FILE_2_NAME = "file-2";

    private ConsolePrinter consolePrinter;
    private DirectoryReader directoryReader;

    @BeforeEach
    public void setUp() throws IOException {
        prepareFile();

        consolePrinter = Mockito.mock(ConsolePrinter.class);

        directoryReader = new DirectoryReader(TEST_FOLDR, consolePrinter);
    }

    @AfterEach
    public void cleanUp() {
        deleteFile(new File(TEST_FOLDR));
    }

    @Test
    public void onList_printsCurrentFolder() {
        directoryReader.onList();

        Mockito.verify(consolePrinter).print("D:" + SUB_DIR_NAME);
        Mockito.verify(consolePrinter).print("D:" + SUB_DIR_2_NAME);
        Mockito.verify(consolePrinter).print("F:" + SUB_FILE_NAME);
        Mockito.verify(consolePrinter).print("F:" + SUB_FILE_2_NAME);
    }

    @Test
    public void onNewFile_shouldCreateNewFile() {
        directoryReader.onNewFile("my-new-file");
        directoryReader.onList();

        Mockito.verify(consolePrinter).print("F:my-new-file");
    }

    private void prepareFile() throws IOException {
        File testDir = new File(TEST_FOLDR);
        testDir.mkdirs();

        File subDir = new File(testDir, SUB_DIR_NAME);
        subDir.mkdir();

        File subDir2 = new File(testDir, SUB_DIR_2_NAME);
        subDir2.mkdir();

        File file = new File(testDir, SUB_FILE_NAME);
        file.createNewFile();

        File file2 = new File(testDir, SUB_FILE_2_NAME);
        file2.createNewFile();
    }

    private void deleteFile(File file) {
        List<File> files = Arrays.asList(file.listFiles());

        for (File innerFile : files) {
            if (innerFile.isDirectory()) {
                deleteFile(innerFile);
                innerFile.delete();
            } else {
                innerFile.delete();
            }
        }
    }
}
