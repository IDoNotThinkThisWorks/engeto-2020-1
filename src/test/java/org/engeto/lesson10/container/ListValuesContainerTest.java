package org.engeto.lesson10.container;

public class ListValuesContainerTest extends ValuesContainerBaseTest {
    ListValuesContainer container = new ListValuesContainer();

    @Override
    protected ValuesContainer getContainer() {
        return container;
    }
}
