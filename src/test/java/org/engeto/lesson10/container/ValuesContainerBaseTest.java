package org.engeto.lesson10.container;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class ValuesContainerBaseTest {
    private ValuesContainer container;

    @BeforeEach
    public void setUp() {
        container = getContainer();
    }

    @Test
    public void getLastValue_returnsInsertedValue() {
        container.insert("value");

        String result = container.getLastValue();

        assertThat(result).isEqualTo("value");
    }

    @Test
    public void getLastValue_returnsLastInstertedValue() {
        container.insert("value");
        container.insert("secondValue");

        String result = container.getLastValue();

        assertThat(result).isEqualTo("secondValue");
    }

    @Test
    public void getValue_returnsCorrectValue() {
        container.insert("value");
        container.insert("secondValue");
        container.insert("thirdValue");

        String result0 = container.getValue(0);
        String result1 = container.getValue(1);
        String result2 = container.getValue(2);

        assertThat(result0).isEqualTo("thirdValue");
        assertThat(result1).isEqualTo("secondValue");
        assertThat(result2).isEqualTo("value");
    }

    @Test
    public void count_returnsCorrectCount() {
        container.insert("value");
        container.insert("secondValue");

        int count = container.count();

        assertThat(count).isEqualTo(2);
    }

    protected abstract ValuesContainer getContainer();
}
