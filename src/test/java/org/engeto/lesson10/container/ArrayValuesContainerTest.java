package org.engeto.lesson10.container;

public class ArrayValuesContainerTest extends ValuesContainerBaseTest {
    ArrayValuesContainer container = new ArrayValuesContainer();

    @Override
    protected ValuesContainer getContainer() {
        return container;
    }
}
